---
# Prepare to build the kernel.

- name: Wait for cloud-init to finish setting up the instance
  raw: test -f /tmp/cloud-init-completed
  retries: 60
  register: cloud_init_check
  until: cloud_init_check is success

- name: Gather facts for instances
  setup:
    gather_timeout: 30

- name: Load variables
  include_vars:
    file: ../vars.yml

- name: Get the arch from the hostname
  set_fact:
    arch: "{{ inventory_hostname.split('_')[-1:] }}"

- debug:
    var: known_arches

- name: Set the arch details for this instance
  set_fact:
    arch_details: "{{ known_arches | selectattr('name', 'equalto', arch) | first }}"

- name: Ensure all packages are updated
  package:
    name: '*'
    state: latest

- name: Install requirements for building the kernel
  package:
    name:
      - bc
      - bison
      - ccache
      - createrepo
      - diffstat
      - elfutils-libelf-devel
      - findutils
      - flex
      - gcc
      - git
      - glibc-devel
      - glibc-static
      - glibc-langpack-en
      - hostname
      - kmod
      - make
      - openssl-devel
      - patchutils
      - procps-ng
      - python3
      - python3-devel
      - quilt
      - rng-tools
      - rsync
      - tar
      - tree
      - vim-minimal
      - xz
      - wget
    state: present

- name: Install cross compiler packages
  package:
    name: "{{ arch_details.cross_compiler_packages }}"
    state: present

- name: Set ccache facts
  set_fact:
    ccache_tarball_filename: "{{ repo_owner }}-{{ repo_name }}-{{ arch_details.name }}.tar"
    ccache_dir: "/opt/ccache"

- name: Create the ccache directory
  file:
    path: "{{ ccache_dir }}"
    state: directory

- name: Check if there is an existing cache in S3
  aws_s3:
    bucket: "{{ s3_ccache_bucket }}"
    mode: list
  register: objects
  delegate_to: localhost

- include_tasks: includes/restore-ccache.yml
  when: ccache_tarball_filename in objects.s3_keys

- include_tasks: "includes/restore-source-{{ make_target }}.yml"
